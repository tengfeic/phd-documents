# 面向工业低功耗有损网络的分布式关键技术研究 

## 引言

* 课题背景

    > 从全世界各个研究机构以及组织对物联网的重视开始，引入对物联网定义的概念， 
    > 继而推动物联网的关键技术：通讯协议的制定。
    > 作为物联网中最广泛的一种网络： 低功耗有损网络LLN， 对其的通讯协议标准的定义显得尤为重要。 
    > 低功耗与高可靠性是对LLN的性能衡量的关键指标,而时间同步与分布式资源分配则是影响这两种指标的关键因素。 

* 研究的意义和目的
    * 工业有线网络的代价很大，无线技术可以很好解决
    * 工业物联网对工业技术难题提供帮助.
    
* 主要内容以及创新点
    * 自适应同步算法
    * 代价权衡资源重分配算法
    * 低时延的资源分配算法
* 论文的组织
## 文献综述
* 工业低功耗有损网络的关键技术概述
    * 时间同步概述
    * 资源分配协议
* 低功耗无线网络时间同步方法
    * 非确定性射频数据传输时延
    * 针对时钟漂移的时间同步方发研究
* 低功耗分布式资源分配方法
    * 基于能量模型的资源分配方法
    * 无冲突的分布式资源分配方法
    * 端到端资源分配机制
    * 加入对这些方法的弊端分析总结，在没给小结之后(?)
* 基于TSCH通信协议的分布式资源分配方法
    * TSCH与IETF 6TiSCH协议栈 **稍微介绍一些RPL,又来选择同步时钟源** 
    * 基于TSCH通信协议的分布式资源分配方法
## 自适应时钟偏差补偿同步算法
* 引言
* 时钟偏差模型
    * 问题定义
    * 有补偿的时钟偏差模型
* 自适应始终补偿同步算法
* 多条网络中同步的难题
* 自适应时钟偏差补偿算法在多条网络中的应用
    * 协调同步
    * 新节点加入与重同步的应对
* 实验结果与分析
    * 单跳网络
    * 多跳网络
## 基于代价权衡的资源重分配算法
* 引言
* TSCH网络中资源的平等性
* 资源重分配的提出
* 实验结果分析
## 低延时的资源分配算法
* 引言
* TSCH中对时延的计算 （ASN）
* 低时延分配算法： 公式
* 实验结果分析
## 结论
去掉"下一步"分析