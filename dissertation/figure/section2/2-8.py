import numpy as np
import matplotlib.pyplot as plt
import random

y1 = [-1,-1.8,-2.7,-3.8,-4.6,\
      -5,-4  ,1   ,   5,   10,\
      11,  13.8,  14.6,  14,   12,\
      10,   7,   4,   2,    1,\
      0.9, 0.5, 0.2, 0.1, -0.5]

fig = plt.figure()
ax = plt.subplot(111)
ax.plot(y1,'-',linewidth=1.5)

# ax.grid(True)

ax.set_ylabel('Temperature (Celsius)',fontsize=22)
ax.set_xlabel('Hour in a Day (Hour)',fontsize=22)
ax.set_ylim(-6,15)
ax.set_xlim(0,24)
fig.savefig('2-7-a.png')

y2 = [14.2,14.8,15.2,15.6,16,\
      14.8,14.0,12  ,   9, 7,\
      7,  6,  4,  4.5,   5,\
      5,   8,   10,   12,    13,\
      13, 13.5, 13.8, 14.0, 14.1]
      
y3 = [13.2,13.6,14.2,14.6,15,\
      14.8,13.8,11.9  ,  10, 8,\
      7.8,  6.9,  6,  7,   8,\
      8.2,   9.4,   10.8,   11.5,  12 ,\
      12.5, 12.8, 13.2, 13.4, 13.5]
      
y4 = [12.2,12.8,13.2,13.6,14,\
      12.8,11.9,10.1, 7.2, 5,\
      4.8,    4, 2.1, 2.5, 3,\
      3.0,    6, 7.8,10.1,11,\
      11.1,12.5,11.9,12.1,12.2]
      
y5 = [13.2,13.5,14.2,14.5,15,\
      14.5,13.9,11.9  ,  10.1, 7.9,\
      7,  6,  4,  4.5,   6,\
      8.1,   9.3,   10.7,   11.4,  12.1 ,\
      12.6, 12.8, 13.1, 13.3, 13.4]

fig = plt.figure()
ax = plt.subplot(111)
ax.plot(y2,'-D',linewidth=1.5,label='Node 1')
ax.plot(y3,'-o',linewidth=1.5,label='Node 2')
ax.plot(y4,'-^',linewidth=1.5,label='Node 3')
ax.plot(y5,'--',linewidth=1.5,label='Average')

ax.set_ylabel('Abosulute Time Drift Rate (ppm)',fontsize=22)
ax.set_xlabel('Hour in a Day (Hour)',fontsize=22)
ax.set_ylim(1,16)
ax.set_xlim(0,24)
ax.legend(loc=3)

fig.savefig('2-7-b.png')

plt.show()