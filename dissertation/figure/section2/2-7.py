import numpy as np
import matplotlib.pyplot as plt
import random

y1 = [random.random()*5 for i in range(35)]
y2 = [random.random()*5 + i*1.8 for i in range(30)]

fig = plt.figure()
ax = plt.subplot(111)
ax.plot(y1+y2,'*')

ax.annotate('time sync was stopped', xy=(35, 10), xycoords='data',
            xytext=(0.56, 0.6), textcoords='axes fraction',
            arrowprops=dict(facecolor='black', shrink=0.05),
            horizontalalignment='center', verticalalignment='top',
            fontsize=15,)

ax.grid(True)

ax.set_xlabel('Time (Minutes)')
ax.set_ylabel('Absolutely Error (us)')
ax.set_xlim(-9,70)
ax.set_ylim(-4,70)

plt.show()